README
================
Joanes Grandjean

``` r
library(spant)
```

    spant 2.4.0


    Attaching package: 'spant'

    The following object is masked from 'package:stats':

        sd

``` r
mrs_data<-read_mrs('assests/meta_nii/FID.nii.gz',format = 'nifti')
plot(mrs_data)
```

![](README_files/figure-gfm/unnamed-chunk-2-1.png)

``` r
mrs_proc<- hsvd_filt(mrs_data,xlim = c(7,6),scale = 'ppm') |> shift(-1.58)
plot(mrs_proc,xlim=c(4.2,0.5)) 
```

![](README_files/figure-gfm/unnamed-chunk-3-1.png)

``` r
basis <- sim_basis_1h_brain_press(mrs_proc)
print(basis)
```

    Basis set parameters
    -------------------------------
    Trans. freq (MHz)       : 500.297057909321
    Data points             : 1980
    Sampling frequency (Hz) : 5000
    Elements                : 27

    Names
    -------------------------------
    -CrCH2,Ala,Asp,Cr,GABA,Glc,Gln,
    GSH,Glu,GPC,Ins,Lac,Lip09,
    Lip13a,Lip13b,Lip20,MM09,MM12,
    MM14,MM17,MM20,NAA,NAAG,PCh,
    PCr,sIns,Tau

``` r
stackplot(basis, xlim = c(4, 0.5), labels = basis$names, y_offset = 5)
```

![](README_files/figure-gfm/unnamed-chunk-4-1.png)

``` r
fit_res <- fit_mrs(mrs_proc, basis, opts = abfit_opts(noise_region = c(6, 8)))
```


      |                                                                            
      |                                                                      |   0%
      |                                                                            
      |======================================================================| 100%

``` r
plot(fit_res)
```

![](README_files/figure-gfm/unnamed-chunk-6-1.png)

``` r
amps <- fit_amps(fit_res)
amps
```

       X.CrCH2 Ala      Asp       Cr     GABA Glc Gln       GSH      Glu      GPC
    1 1.986017   0 2.556492 4.622771 14.38956   0   0 0.9498425 19.72293 1.945786
           Ins      Lac    Lip09   Lip13a Lip13b    Lip20 MM09     MM12     MM14
    1 15.70549 1.163871 12.35358 3.166892      0 16.78848    0 7.719185 17.96341
          MM17     MM20      NAA     NAAG      PCh      PCr sIns      Tau     tNAA
    1 13.14315 73.69718 3.492013 1.190705 1.089337 3.112748    0 18.47506 4.682719
          tCr     tCho      Glx    tLM09    tLM13    tLM20
    1 7.73552 3.035123 19.72293 12.35358 28.84949 90.48566

``` r
lcbasis <- read_basis_ac('assests/basis/BiospecPRESS_16ms11T7.BASIS')
print(lcbasis)
```

    Basis set parameters
    -------------------------------
    Trans. freq (MHz)       : 500.341003
    Data points             : 8192
    Sampling frequency (Hz) : 5988.02373695723
    Elements                : 25

    Names
    -------------------------------
    2HG,Ace,Ala,Asp,Cho,Cre,GABA,
    Glc,Gln,Glu,Gly,GPC,GSH,Lac,mI,
    NAA,NAAG,NPC,PCh,PCr,PE,Pyr,
    Scyllo,Suc,Tau

``` r
stackplot(lcbasis, xlim = c(4, 0.5), labels = basis$names, y_offset = 5)
```

![](README_files/figure-gfm/unnamed-chunk-8-1.png)

``` r
#fit_res_lc <- fit_mrs(mrs_data, lcbasis, opts = abfit_opts(noise_region = c(6, 8)))
#plot(fit_res_lc)
```
